#include <ESP32CAN.h>
#include <CAN_config.h>

//#define PCB
#if defined(PCB)
#define EN1          25
#define EN2          33
#define TX           GPIO_NUM_32
#define RX           GPIO_NUM_35
#else
#define EN1          25
#define EN2          32
#define TX           GPIO_NUM_33
#define RX           GPIO_NUM_34
#endif

#if defined (PCB)
void SelectCan(uint8_t can) {
  switch (can) {
    case 0:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, HIGH);
      Serial.println("Can Off.");
      break;
    case 1:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, LOW);
      Serial.println("EV Can Selected.");
      break;
    case 2:
      digitalWrite(EN1, LOW);
      digitalWrite(EN2, HIGH);
      Serial.println("Car Can Selected.");
      break;
  }
}
#else

CAN_device_t CAN_cfg;
void SelectCan(uint8_t can) {
  switch (can) {
    case 0:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, HIGH);
      Serial.println("Can Off.");
      break;
    case 1:
      digitalWrite(EN1, LOW);
      digitalWrite(EN2, HIGH);
      Serial.println("EV Can Selected.");
      break;
    case 2:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, LOW);
      Serial.println("Car Can Selected.");
      break;
  }
}
#endif

void setup() {
  Serial.begin(115200);
  //SerialBT.begin("ESP32test");
  Serial.println(F("iotsharing.com CAN read demo"));
  CAN_cfg.speed = CAN_SPEED_500KBPS;

  pinMode(EN1, OUTPUT);
  pinMode(EN2, OUTPUT);
  SelectCan(0);
  CAN_cfg.tx_pin_id = TX; 
  CAN_cfg.rx_pin_id = RX; 
  Serial.println(F("connected"));
  Serial.flush();
  SelectCan(1);
  CAN_cfg.rx_queue = xQueueCreate(10, sizeof(CAN_frame_t));
  ESP32Can.CANInit();
}

void loop() {

  CAN_frame_t rx_frame;
  //receive next CAN frame from queue
  if (xQueueReceive(CAN_cfg.rx_queue, &rx_frame, 3 * portTICK_PERIOD_MS) == pdTRUE) {

    Serial.printf(" from 0x%x, DLC %d: ", rx_frame.MsgID,  rx_frame.FIR.B.DLC);
    for (int i = 0; i < 8; i++) {
      Serial.printf("%x\t", rx_frame.data.u8[i]);
    }
    Serial.printf("\n");
  }

}
